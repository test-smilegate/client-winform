#pragma once
#include <string>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <iostream>
using namespace std;

delegate void CallbackRequest(bool, string, string);

ref class Client
{
public:
	Client()
	{
		_privateInstance = this;
	};
	CallbackRequest^ onLogin;
	CallbackRequest^ onRegister;
	static void fire_event_login(bool success, string token, string message)
	{
		_privateInstance->onLogin(success, token, message);
	}
	static void fire_event_register(bool success, string token, string message)
	{
		_privateInstance->onRegister(success, token, message);
	}

	void login(string id, string pwd);
	void register_id(string id, string pwd);

protected:
	static Client^ _privateInstance;
	void call_request(string url, string body, string req_type, curlpp::types::WriteFunctionFunctor callback);
};
