#include "Client.h"
#include "json_spirit/json_spirit.h"
#include <iostream>
#include <string>
#include "MainUI.h"

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>



using namespace System;
using namespace std;
using namespace json_spirit;
using namespace curlpp::options;
using namespace UI;


const string url = "http://localhost:8765";
const string url_login = "/api/login";
const string url_register = "/api/register";


namespace
{
	char *data = NULL;
	size_t writeDataLogin(char *buffer, size_t size, size_t nitems)
	{
		json_spirit::Value value;
		if (json_spirit::read(buffer, value))
		{
			auto obj = value.get_obj();

			cout << endl << json_spirit::write(obj) << endl;;
			bool successful = false;
			string token, message;

			for (auto entry : obj)
			{
				if (entry.name_ == "status_code")
				{
					successful = entry.value_.get_int() == 0;
				}
				else if (entry.name_ == "token")
				{
					token = entry.value_.get_str();
				}
				else if (entry.name_ == "message")
				{
					message = entry.value_.get_str();
				}
			}
			Client::fire_event_login(successful, token, message);

		}

		return size * nitems;
	}
	size_t writeDataRegister(char *buffer, size_t size, size_t nitems)
	{
		json_spirit::Value value;
		if (json_spirit::read(buffer, value))
		{
			auto obj = value.get_obj();
			bool successful = false;
			string token, message;
			for (auto entry : obj)
			{
				if (entry.name_ == "status_code")
				{
					successful = entry.value_.get_int() == 0;
				}
				else if (entry.name_ == "token")
				{
					token = entry.value_.get_str();
				}
				else if (entry.name_ == "message")
				{
					message = entry.value_.get_str();
				}
			}
			Client::fire_event_register(successful, token, message);
		}
		return size * nitems;
	}
}

void Client::login(string id, string pwd)
{

	json_spirit::Object body;
	body.push_back(Pair("id", id));
	body.push_back(Pair("pwd", pwd));

	auto str_body = json_spirit::write(body);

	call_request(url + url_login, str_body, "POST", curlpp::types::WriteFunctionFunctor(writeDataLogin));
}

void Client::register_id(string id, string pwd)
{
	json_spirit::Object body;
	body.push_back(Pair("id", id));
	body.push_back(Pair("pwd", pwd));

	auto str_body = json_spirit::write(body);
	call_request(url + url_register, str_body, "POST", curlpp::types::WriteFunctionFunctor(writeDataRegister));
}

void Client::call_request(string url, string body, string req_type, curlpp::types::WriteFunctionFunctor callback)
{
	try
	{
		curlpp::Cleanup cleanup;
		curlpp::Easy request;
		request.setOpt<Url>(url);
		request.setOpt<Port>(8765);
		request.setOpt<Header>(new HttpHeader({ "Content-Type:application/json" }));
		request.setOpt(new WriteFunction(callback));
		request.setOpt(new CustomRequest(req_type));
		request.setOpt(new PostFields(body));
		request.perform();
	}
	catch (curlpp::LogicError & e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (curlpp::RuntimeError & e)
	{
		std::cout << e.what() << std::endl;
	}
}
