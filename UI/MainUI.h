#pragma once
#include <iostream>
#include <string>
#include <msclr\marshal_cppstd.h>
#include "Client.h"
#include <Windows.h>


void startup(LPCTSTR lpApplicationName)
{
	// additional information
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	cout << endl << "Open Game: " << lpApplicationName;
	// start the program up
	CreateProcess(lpApplicationName,   // the path
		NULL,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi             // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
	);
	WaitForSingleObject(pi.hProcess, INFINITE);
	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}



namespace UI {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Diagnostics;

	using namespace std;


	/// <summary>
	/// Summary for MainUI
	/// </summary>

	public ref class MainUI : public System::Windows::Forms::Form
	{

	public:
		Client^ client;
		MainUI(void)
		{
			_privateInstance = this;
			list_folder_game = gcnew System::Collections::Generic::List<String^>();
			client = gcnew Client();
			client->onLogin = gcnew CallbackRequest(static_callback_login);
			client->onRegister = gcnew CallbackRequest(static_callback_register);

			InitializeComponent();
			LoadGames();
			//
			//TODO: Add the constructor code here
			//
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainUI()
		{
			if (components)
			{
				delete components;
			}
		}

	protected: System::Windows::Forms::TextBox^  txt_id;
	protected: System::Windows::Forms::Label^  label_id;
	protected: System::Windows::Forms::TextBox^  txt_password;
	protected: System::Windows::Forms::Label^  label_password;
	protected: System::Windows::Forms::Label^  label_logo;
	protected: System::Windows::Forms::Button^  btn_login;
	protected: System::Windows::Forms::Button^  btn_register;
	private: System::Windows::Forms::Button^  btn_run_game;
	protected:

	protected:
		System::Collections::Generic::List<String^>^ list_folder_game;
		void LoadGames();


	private:
		static MainUI^ _privateInstance;
	private: System::Windows::Forms::Panel^  panel_login;
	private: System::Windows::Forms::Panel^  panel_register;
	private: System::Windows::Forms::Button^  btn_register_register;
	private: System::Windows::Forms::TextBox^  txt_pwd_confirm_register;
	private: System::Windows::Forms::TextBox^  txt_pwd_register;
	private: System::Windows::Forms::TextBox^  txt_id_register;
	private: System::Windows::Forms::Label^  label_pwd_confirm_register;
	private: System::Windows::Forms::Label^  label_pwd_register;
	private: System::Windows::Forms::Label^  label_id_register;
	private: System::Windows::Forms::Label^  label_register_title;
	private: System::Windows::Forms::Button^  btn_back_register;
	private: System::Windows::Forms::Panel^  panel_list_game;
	private: System::Windows::Forms::Label^  label_user_name_content;
	private: System::Windows::Forms::Label^  label_user_name;
	private: System::Windows::Forms::ListView^  lv_list_game;

	private: System::Windows::Forms::Label^  label_list_game;
			 System::String^ login_token;

			 /// <summary>
		/// Required designer variable.
		/// </summary>
			 System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// Required method for Designer support - do not modify
			 /// the contents of this method with the code editor.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->txt_id = (gcnew System::Windows::Forms::TextBox());
				 this->label_id = (gcnew System::Windows::Forms::Label());
				 this->txt_password = (gcnew System::Windows::Forms::TextBox());
				 this->label_password = (gcnew System::Windows::Forms::Label());
				 this->label_logo = (gcnew System::Windows::Forms::Label());
				 this->btn_login = (gcnew System::Windows::Forms::Button());
				 this->btn_register = (gcnew System::Windows::Forms::Button());
				 this->panel_login = (gcnew System::Windows::Forms::Panel());
				 this->panel_list_game = (gcnew System::Windows::Forms::Panel());
				 this->lv_list_game = (gcnew System::Windows::Forms::ListView());
				 this->label_list_game = (gcnew System::Windows::Forms::Label());
				 this->label_user_name_content = (gcnew System::Windows::Forms::Label());
				 this->label_user_name = (gcnew System::Windows::Forms::Label());
				 this->panel_register = (gcnew System::Windows::Forms::Panel());
				 this->btn_back_register = (gcnew System::Windows::Forms::Button());
				 this->btn_register_register = (gcnew System::Windows::Forms::Button());
				 this->txt_pwd_confirm_register = (gcnew System::Windows::Forms::TextBox());
				 this->txt_pwd_register = (gcnew System::Windows::Forms::TextBox());
				 this->txt_id_register = (gcnew System::Windows::Forms::TextBox());
				 this->label_pwd_confirm_register = (gcnew System::Windows::Forms::Label());
				 this->label_pwd_register = (gcnew System::Windows::Forms::Label());
				 this->label_id_register = (gcnew System::Windows::Forms::Label());
				 this->label_register_title = (gcnew System::Windows::Forms::Label());
				 this->btn_run_game = (gcnew System::Windows::Forms::Button());
				 this->panel_login->SuspendLayout();
				 this->panel_list_game->SuspendLayout();
				 this->panel_register->SuspendLayout();
				 this->SuspendLayout();
				 // 
				 // txt_id
				 // 
				 this->txt_id->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->txt_id->Location = System::Drawing::Point(228, 135);
				 this->txt_id->MaxLength = 16;
				 this->txt_id->Name = L"txt_id";
				 this->txt_id->Size = System::Drawing::Size(100, 20);
				 this->txt_id->TabIndex = 0;
				 this->txt_id->TextChanged += gcnew System::EventHandler(this, &MainUI::txt_id_TextChanged);
				 // 
				 // label_id
				 // 
				 this->label_id->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_id->AutoSize = true;
				 this->label_id->Location = System::Drawing::Point(191, 138);
				 this->label_id->Name = L"label_id";
				 this->label_id->Size = System::Drawing::Size(18, 13);
				 this->label_id->TabIndex = 1;
				 this->label_id->Text = L"ID";
				 this->label_id->Click += gcnew System::EventHandler(this, &MainUI::label_id_Click);
				 // 
				 // txt_password
				 // 
				 this->txt_password->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->txt_password->Location = System::Drawing::Point(228, 177);
				 this->txt_password->MaxLength = 20;
				 this->txt_password->Name = L"txt_password";
				 this->txt_password->Size = System::Drawing::Size(100, 20);
				 this->txt_password->TabIndex = 2;
				 this->txt_password->UseSystemPasswordChar = true;
				 this->txt_password->TextChanged += gcnew System::EventHandler(this, &MainUI::txt_password_TextChanged);
				 // 
				 // label_password
				 // 
				 this->label_password->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_password->AutoSize = true;
				 this->label_password->Location = System::Drawing::Point(156, 180);
				 this->label_password->Name = L"label_password";
				 this->label_password->Size = System::Drawing::Size(53, 13);
				 this->label_password->TabIndex = 3;
				 this->label_password->Text = L"Password";
				 // 
				 // label_logo
				 // 
				 this->label_logo->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_logo->AutoSize = true;
				 this->label_logo->Font = (gcnew System::Drawing::Font(L"Noto Sans JP", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label_logo->Location = System::Drawing::Point(187, 52);
				 this->label_logo->Name = L"label_logo";
				 this->label_logo->Size = System::Drawing::Size(174, 39);
				 this->label_logo->TabIndex = 4;
				 this->label_logo->Text = L"SMITE GATE";
				 // 
				 // btn_login
				 // 
				 this->btn_login->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->btn_login->Location = System::Drawing::Point(238, 203);
				 this->btn_login->Name = L"btn_login";
				 this->btn_login->Size = System::Drawing::Size(80, 25);
				 this->btn_login->TabIndex = 5;
				 this->btn_login->Text = L"Login";
				 this->btn_login->UseVisualStyleBackColor = true;
				 this->btn_login->Click += gcnew System::EventHandler(this, &MainUI::btn_login_Click);
				 // 
				 // btn_register
				 // 
				 this->btn_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->btn_register->Location = System::Drawing::Point(238, 234);
				 this->btn_register->Name = L"btn_register";
				 this->btn_register->Size = System::Drawing::Size(80, 25);
				 this->btn_register->TabIndex = 6;
				 this->btn_register->Text = L"Register";
				 this->btn_register->UseVisualStyleBackColor = true;
				 this->btn_register->Click += gcnew System::EventHandler(this, &MainUI::btn_register_Click);
				 // 
				 // panel_login
				 // 
				 this->panel_login->BackColor = System::Drawing::SystemColors::Control;
				 this->panel_login->Controls->Add(this->txt_id);
				 this->panel_login->Controls->Add(this->btn_login);
				 this->panel_login->Controls->Add(this->btn_register);
				 this->panel_login->Controls->Add(this->label_logo);
				 this->panel_login->Controls->Add(this->label_password);
				 this->panel_login->Controls->Add(this->txt_password);
				 this->panel_login->Controls->Add(this->label_id);
				 this->panel_login->Location = System::Drawing::Point(3, 3);
				 this->panel_login->Name = L"panel_login";
				 this->panel_login->Size = System::Drawing::Size(600, 350);
				 this->panel_login->TabIndex = 7;
				 // 
				 // panel_list_game
				 // 
				 this->panel_list_game->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->panel_list_game->Controls->Add(this->btn_run_game);
				 this->panel_list_game->Controls->Add(this->lv_list_game);
				 this->panel_list_game->Controls->Add(this->label_list_game);
				 this->panel_list_game->Controls->Add(this->label_user_name_content);
				 this->panel_list_game->Controls->Add(this->label_user_name);
				 this->panel_list_game->Location = System::Drawing::Point(9, 9);
				 this->panel_list_game->Name = L"panel_list_game";
				 this->panel_list_game->Size = System::Drawing::Size(560, 335);
				 this->panel_list_game->TabIndex = 7;
				 this->panel_list_game->Visible = false;
				 // 
				 // lv_list_game
				 // 
				 this->lv_list_game->HideSelection = false;
				 this->lv_list_game->Location = System::Drawing::Point(109, 72);
				 this->lv_list_game->Name = L"lv_list_game";
				 this->lv_list_game->Size = System::Drawing::Size(361, 196);
				 this->lv_list_game->TabIndex = 3;
				 this->lv_list_game->UseCompatibleStateImageBehavior = false;
				 this->lv_list_game->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MainUI::list_game_mouse_click);
				 this->lv_list_game->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainUI::list_game_mouse_down);
				 // 
				 // label_list_game
				 // 
				 this->label_list_game->AutoSize = true;
				 this->label_list_game->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label_list_game->Location = System::Drawing::Point(224, 43);
				 this->label_list_game->Name = L"label_list_game";
				 this->label_list_game->Size = System::Drawing::Size(100, 25);
				 this->label_list_game->TabIndex = 2;
				 this->label_list_game->Text = L"List Game";
				 // 
				 // label_user_name_content
				 // 
				 this->label_user_name_content->AutoSize = true;
				 this->label_user_name_content->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular,
					 System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
				 this->label_user_name_content->ForeColor = System::Drawing::SystemColors::MenuHighlight;
				 this->label_user_name_content->Location = System::Drawing::Point(72, 5);
				 this->label_user_name_content->Name = L"label_user_name_content";
				 this->label_user_name_content->Size = System::Drawing::Size(72, 17);
				 this->label_user_name_content->TabIndex = 1;
				 this->label_user_name_content->Text = L"Some one";
				 this->label_user_name_content->Click += gcnew System::EventHandler(this, &MainUI::label_user_name_content_Click);
				 // 
				 // label_user_name
				 // 
				 this->label_user_name->AutoSize = true;
				 this->label_user_name->Location = System::Drawing::Point(3, 7);
				 this->label_user_name->Name = L"label_user_name";
				 this->label_user_name->Size = System::Drawing::Size(63, 13);
				 this->label_user_name->TabIndex = 0;
				 this->label_user_name->Text = L"User Name:";
				 // 
				 // panel_register
				 // 
				 this->panel_register->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
					 | System::Windows::Forms::AnchorStyles::Left)
					 | System::Windows::Forms::AnchorStyles::Right));
				 this->panel_register->BackColor = System::Drawing::SystemColors::Control;
				 this->panel_register->Controls->Add(this->btn_back_register);
				 this->panel_register->Controls->Add(this->btn_register_register);
				 this->panel_register->Controls->Add(this->txt_pwd_confirm_register);
				 this->panel_register->Controls->Add(this->txt_pwd_register);
				 this->panel_register->Controls->Add(this->txt_id_register);
				 this->panel_register->Controls->Add(this->label_pwd_confirm_register);
				 this->panel_register->Controls->Add(this->label_pwd_register);
				 this->panel_register->Controls->Add(this->label_id_register);
				 this->panel_register->Controls->Add(this->label_register_title);
				 this->panel_register->Location = System::Drawing::Point(9, 13);
				 this->panel_register->Name = L"panel_register";
				 this->panel_register->Size = System::Drawing::Size(560, 337);
				 this->panel_register->TabIndex = 8;
				 this->panel_register->Visible = false;
				 // 
				 // btn_back_register
				 // 
				 this->btn_back_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->btn_back_register->Location = System::Drawing::Point(224, 255);
				 this->btn_back_register->Name = L"btn_back_register";
				 this->btn_back_register->Size = System::Drawing::Size(80, 25);
				 this->btn_back_register->TabIndex = 15;
				 this->btn_back_register->Text = L"Back";
				 this->btn_back_register->UseVisualStyleBackColor = true;
				 this->btn_back_register->Click += gcnew System::EventHandler(this, &MainUI::btn_back_register_click);
				 // 
				 // btn_register_register
				 // 
				 this->btn_register_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->btn_register_register->Location = System::Drawing::Point(224, 224);
				 this->btn_register_register->Name = L"btn_register_register";
				 this->btn_register_register->Size = System::Drawing::Size(80, 25);
				 this->btn_register_register->TabIndex = 14;
				 this->btn_register_register->Text = L"Register";
				 this->btn_register_register->UseVisualStyleBackColor = true;
				 this->btn_register_register->Click += gcnew System::EventHandler(this, &MainUI::btn_register_register_Click);
				 // 
				 // txt_pwd_confirm_register
				 // 
				 this->txt_pwd_confirm_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->txt_pwd_confirm_register->Location = System::Drawing::Point(215, 184);
				 this->txt_pwd_confirm_register->Name = L"txt_pwd_confirm_register";
				 this->txt_pwd_confirm_register->Size = System::Drawing::Size(100, 20);
				 this->txt_pwd_confirm_register->TabIndex = 13;
				 this->txt_pwd_confirm_register->UseSystemPasswordChar = true;
				 this->txt_pwd_confirm_register->TextChanged += gcnew System::EventHandler(this, &MainUI::txt_pwd_confirm_register_TextChanged);
				 // 
				 // txt_pwd_register
				 // 
				 this->txt_pwd_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->txt_pwd_register->Location = System::Drawing::Point(215, 148);
				 this->txt_pwd_register->Name = L"txt_pwd_register";
				 this->txt_pwd_register->Size = System::Drawing::Size(100, 20);
				 this->txt_pwd_register->TabIndex = 12;
				 this->txt_pwd_register->UseSystemPasswordChar = true;
				 // 
				 // txt_id_register
				 // 
				 this->txt_id_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->txt_id_register->Location = System::Drawing::Point(215, 115);
				 this->txt_id_register->Name = L"txt_id_register";
				 this->txt_id_register->Size = System::Drawing::Size(100, 20);
				 this->txt_id_register->TabIndex = 11;
				 this->txt_id_register->TextChanged += gcnew System::EventHandler(this, &MainUI::txt_id_register_TextChanged);
				 // 
				 // label_pwd_confirm_register
				 // 
				 this->label_pwd_confirm_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_pwd_confirm_register->AutoSize = true;
				 this->label_pwd_confirm_register->Location = System::Drawing::Point(109, 187);
				 this->label_pwd_confirm_register->Name = L"label_pwd_confirm_register";
				 this->label_pwd_confirm_register->Size = System::Drawing::Size(90, 13);
				 this->label_pwd_confirm_register->TabIndex = 10;
				 this->label_pwd_confirm_register->Text = L"Confirm password";
				 this->label_pwd_confirm_register->Click += gcnew System::EventHandler(this, &MainUI::label_pwd_confirm_register_Click);
				 // 
				 // label_pwd_register
				 // 
				 this->label_pwd_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_pwd_register->AutoSize = true;
				 this->label_pwd_register->Location = System::Drawing::Point(146, 151);
				 this->label_pwd_register->Name = L"label_pwd_register";
				 this->label_pwd_register->Size = System::Drawing::Size(53, 13);
				 this->label_pwd_register->TabIndex = 9;
				 this->label_pwd_register->Text = L"Password";
				 // 
				 // label_id_register
				 // 
				 this->label_id_register->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_id_register->AutoSize = true;
				 this->label_id_register->Location = System::Drawing::Point(183, 118);
				 this->label_id_register->Name = L"label_id_register";
				 this->label_id_register->Size = System::Drawing::Size(16, 13);
				 this->label_id_register->TabIndex = 8;
				 this->label_id_register->Text = L"Id";
				 this->label_id_register->Click += gcnew System::EventHandler(this, &MainUI::label_id_register_Click);
				 // 
				 // label_register_title
				 // 
				 this->label_register_title->Anchor = System::Windows::Forms::AnchorStyles::Top;
				 this->label_register_title->AutoSize = true;
				 this->label_register_title->Font = (gcnew System::Drawing::Font(L"Noto Sans JP", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label_register_title->Location = System::Drawing::Point(217, 57);
				 this->label_register_title->Name = L"label_register_title";
				 this->label_register_title->Size = System::Drawing::Size(98, 30);
				 this->label_register_title->TabIndex = 0;
				 this->label_register_title->Text = L"Register";
				 // 
				 // btn_run_game
				 // 
				 this->btn_run_game->Location = System::Drawing::Point(249, 290);
				 this->btn_run_game->Name = L"btn_run_game";
				 this->btn_run_game->Size = System::Drawing::Size(75, 23);
				 this->btn_run_game->TabIndex = 4;
				 this->btn_run_game->Text = L"Run Game";
				 this->btn_run_game->UseVisualStyleBackColor = true;
				 this->btn_run_game->Visible = false;
				 this->btn_run_game->Click += gcnew System::EventHandler(this, &UI::MainUI::btn_run_game_click);
				 // 
				 // MainUI
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(584, 361);
				 this->Controls->Add(this->panel_list_game);
				 this->Controls->Add(this->panel_login);
				 this->Controls->Add(this->panel_register);
				 this->Name = L"MainUI";
				 this->Text = L"MainUI";
				 this->panel_login->ResumeLayout(false);
				 this->panel_login->PerformLayout();
				 this->panel_list_game->ResumeLayout(false);
				 this->panel_list_game->PerformLayout();
				 this->panel_register->ResumeLayout(false);
				 this->panel_register->PerformLayout();
				 this->ResumeLayout(false);

			 }
#pragma endregion

	private: System::Void txt_password_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{

	}

	private: System::Void txt_id_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{

	}

	private: System::Void btn_login_Click(System::Object^  sender, System::EventArgs^  e)
	{
		LogString("Click login");
		if (System::String::IsNullOrEmpty(txt_password->Text)
			|| System::String::IsNullOrEmpty(txt_id->Text))
		{
			MessageBox::Show("Field cannot be empty");
			return;
		}
		auto pwd = msclr::interop::marshal_as<std::string>(txt_password->Text);
		auto id = msclr::interop::marshal_as<std::string>(txt_id->Text);
		client->login(id, pwd);
	}

	private: System::Void btn_register_Click(System::Object^  sender, System::EventArgs^  e)
	{
		panel_login->Hide();
		panel_register->Show();
	}

	private: System::Void LogString(String^ str)
	{
		msclr::interop::marshal_context context;
		string log = context.marshal_as<string>(str);
		cout << log << endl;
	}

	public:
		System::Void callback_login(bool success, string token, string message)
		{
			if (success)
			{
				//do something;
				login_token = gcnew System::String(token.c_str());
				label_user_name_content->Text = txt_id->Text;
				panel_login->Hide();
				panel_list_game->Show();
			}
			else
			{
				//show alert dialog;
				System::String^ m = gcnew System::String(message.c_str());
				MessageBox::Show(m);
			}
		}
		void callback_register(bool success, string token, string message)
		{
			if (success)
			{
				MessageBox::Show("Register successful");
				panel_register->Hide();
				panel_login->Show();
				txt_id->Text = txt_id_register->Text;
				txt_id_register->Clear();
				txt_pwd_confirm_register->Clear();
				txt_pwd_register->Clear();
			}
			else
			{
				System::String^ m = gcnew System::String(message.c_str());
				MessageBox::Show(m);
			}
		}
	private:
		static void static_callback_login(bool success, string token, string message)
		{
			_privateInstance->callback_login(success, token, message);
		}

		static void static_callback_register(bool success, string token, string message)
		{
			_privateInstance->callback_register(success, token, message);
		}

	private: System::Void label_id_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void btn_register_register_Click(System::Object^  sender, System::EventArgs^  e)
	{
		LogString("Click register");
		if (System::String::IsNullOrEmpty(txt_pwd_confirm_register->Text)
			|| System::String::IsNullOrEmpty(txt_id_register->Text)
			|| System::String::IsNullOrEmpty(txt_pwd_register->Text))
		{
			MessageBox::Show("Field cannot be empty");
			return;
		}
		if (txt_pwd_confirm_register->Text != txt_pwd_confirm_register->Text)
		{
			MessageBox::Show("Password did not match");
			return;
		}
		auto pwd = msclr::interop::marshal_as<std::string>(txt_pwd_register->Text);
		auto id = msclr::interop::marshal_as<std::string>(txt_id_register->Text);
		client->register_id(id, pwd);
	}
	private: System::Void btn_back_register_click(System::Object^  sender, System::EventArgs^  e)
	{
		panel_register->Hide();
		panel_login->Show();
	}

	private: System::Void label_pwd_confirm_register_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void txt_id_register_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void txt_pwd_confirm_register_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label_id_register_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label_user_name_content_Click(System::Object^  sender, System::EventArgs^  e)
	{
	}


	private: System::Void list_game_double_click(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		auto info = this->lv_list_game->HitTest(e->X, e->Y);
		ListViewItem^ item = info->Item;
		if (item != nullptr)
		{
			LogString(item->Text);
		}
	}
	private: System::Void list_game_mouse_click(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		auto info = this->lv_list_game->HitTest(e->X, e->Y);
		ListViewItem^ item = info->Item;
		if (item != nullptr)
		{
			LogString(item->Text);
		}
		else
		{

		}
	}
	private: System::Void list_game_mouse_down(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		auto info = this->lv_list_game->HitTest(e->X, e->Y);
		ListViewItem^ item = info->Item;
		if (item != nullptr)
		{

			btn_run_game->Show();
		}
		else
		{
			this->lv_list_game->SelectedItems->Clear();
			btn_run_game->Hide();
		}
	}
	private: System::Void btn_run_game_click(System::Object^  sender, System::EventArgs^  e)
	{
		if (this->lv_list_game->SelectedItems->Count > 0)
		{
			auto name = this->lv_list_game->SelectedItems[0]->Text;
			for (int i = 0; i < list_folder_game->Count; i++)
			{
				if (list_folder_game[i]->Contains(name))
				{
					auto path = list_folder_game[i] + "\\" + name + ".exe";
					//std::string str_path = msclr::interop::marshal_as<std::string>(path);
					LogString(path);
					LogString(login_token);
					Process::Start(path," -token "+ login_token);
					break;
				}
			}
		}
	}
	};
};